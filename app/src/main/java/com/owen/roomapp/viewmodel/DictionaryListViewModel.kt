package com.owen.roomapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import com.owen.roomapp.RoomApp
import com.owen.roomapp.database.entity.DictionaryEntity

class DictionaryListViewModel(application: Application) : AndroidViewModel(application) {

    // MediatorLiveData can observe other LiveData objects and react on their emissions.
    private var mObservableDictionaries: MediatorLiveData<List<DictionaryEntity>> = MediatorLiveData()

    init {
        // default null
        mObservableDictionaries.value = null
        val dictionaries = (application as RoomApp).getRepository().loadDictionaries()
        mObservableDictionaries.addSource(dictionaries, mObservableDictionaries::setValue)
    }

    /**
     * Expose the LiveData Products query so the UI can observe it.
     */
    fun getDictionaries(): LiveData<List<DictionaryEntity>> {
        return mObservableDictionaries
    }
}