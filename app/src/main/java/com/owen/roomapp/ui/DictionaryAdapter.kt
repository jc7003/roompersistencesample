package com.owen.roomapp.ui

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.owen.roomapp.R
import com.owen.roomapp.database.entity.DictionaryEntity
import com.owen.roomapp.databinding.ItemDictionaryBinding
import com.owen.roomapp.model.Dictionary

class DictionaryAdapter(private val callback: Dictionary.DictionaryClickCallback): RecyclerView.Adapter<DictionaryAdapter.DictionaryViewHolder>() {

    class DictionaryViewHolder(val binding: ItemDictionaryBinding) : RecyclerView.ViewHolder(binding.root)

    var mDictionaryList: List<Dictionary>? = null
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DictionaryAdapter.DictionaryViewHolder {
        val binding: ItemDictionaryBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.item_dictionary, parent, false)

        binding.callback = this.callback

        return DictionaryViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return mDictionaryList?.size ?: 0
    }

    override fun onBindViewHolder(holder: DictionaryAdapter.DictionaryViewHolder, position: Int) {
        holder.binding.dictionary = mDictionaryList!![position]
        holder.binding.executePendingBindings()
    }
}