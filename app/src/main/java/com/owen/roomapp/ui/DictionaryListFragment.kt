package com.owen.roomapp.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.owen.roomapp.R
import com.owen.roomapp.RoomApp
import com.owen.roomapp.database.entity.DictionaryEntity
import com.owen.roomapp.databinding.ItemDictionaryBinding
import com.owen.roomapp.model.Dictionary
import com.owen.roomapp.viewmodel.DictionaryListViewModel

class DictionaryListFragment : Fragment() {
    companion object {
        const val TAG = "DictionaryListFragment";
    }

    private lateinit var mDictionaryAdapter: DictionaryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.list_fragment, container, false)
//        val root = inflater.inflate(R.layout.list_fragment, container)
//        val recyclerView = root.findViewById<RecyclerView>(R.id.recycler_view)
        mDictionaryAdapter = DictionaryAdapter(object : Dictionary.DictionaryClickCallback {
            override fun onClick(dictionary: Dictionary) {
                Log.d(TAG, "onClick dictionary: ${dictionary.getTitle()}")
            }
        })
        root.findViewById<RecyclerView>(R.id.recycler_view).adapter = mDictionaryAdapter

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel = ViewModelProviders.of(this).get(DictionaryListViewModel::class.java)
        subscribeUi(viewModel)
//        val repository = (activity!!.application as RoomApp).getRepository()
//        val list= repository.loadDictionaries()
//        mDictionaryAdapter.mDictionaryList = list.value
    }

    private fun subscribeUi(viewModel: DictionaryListViewModel) {
        // Update the list when the data changes
        viewModel.getDictionaries().observe(this, Observer<List<DictionaryEntity>> { myProducts ->
            Log.d(TAG, "onChanged")
            if (myProducts != null) {
//                mBinding.setIsLoading(false)
                mDictionaryAdapter.mDictionaryList = myProducts
            } else {
//                mBinding.setIsLoading(true)
            }
            // espresso does not know how to wait for data binding's loop so we execute changes
            // sync.
//            mBinding.executePendingBindings()
        })
    }
}