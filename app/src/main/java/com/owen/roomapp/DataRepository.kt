package com.owen.roomapp

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import com.owen.roomapp.database.AppDatabase
import com.owen.roomapp.database.entity.DictionaryEntity
import com.owen.roomapp.database.entity.IntegrationQuery
import com.owen.roomapp.database.entity.WordEntity

class DataRepository private constructor(private val mDatabase: AppDatabase) {

    private val mObservableWords: MediatorLiveData<List<WordEntity>> = MediatorLiveData()
    private val mObservableDictionary: MediatorLiveData<List<DictionaryEntity>> = MediatorLiveData()

    companion object {

        private var sInstance: DataRepository? = null

        fun getInstance(database: AppDatabase): DataRepository {
            if (sInstance == null) {
                synchronized(DataRepository::class.java) {
                    if (sInstance == null) {
                        sInstance = DataRepository(database)
                    }
                }
            }
            return sInstance!!
        }
    }

    /**
     * Get the list of products from the database and get notified when the data changes.
     */
    val words: LiveData<List<WordEntity>> get() = mObservableWords

    init {
        mObservableWords.addSource(mDatabase.wordDAO().loadAllWords()) { wordEntities ->
            mDatabase.getDatabaseCreated().value?.run {
                mObservableWords.postValue(wordEntities)
            }
        }
        mObservableDictionary.addSource(mDatabase.dictionaryDAO().loadAllDictionaries()) { dictionaryEntities ->
            mDatabase.getDatabaseCreated().value?.run {
                mObservableDictionary.postValue(dictionaryEntities)
            }
        }
    }

    fun loadWords(wordId: Int): LiveData<WordEntity> {
        return mDatabase.wordDAO().loadWord(wordId)
    }

    fun insertWord(words: WordEntity) {
        mDatabase.getExecutors().diskIO().execute {
            mDatabase.wordDAO().insert(words)
        }
    }

    fun deleteWord(words: WordEntity) {
        mDatabase.getExecutors().diskIO().execute {
            mDatabase.wordDAO().delete(words)
        }
    }

    fun insertWords(words: List<WordEntity>) {
        mDatabase.getExecutors().diskIO().execute {
            mDatabase.wordDAO().insertAll(words)
        }
    }

    fun loadDictionaries(): LiveData<List<DictionaryEntity>> {
        return mDatabase.dictionaryDAO().loadAllDictionaries()
    }

    fun insertDictionaries(dictionaries: List<DictionaryEntity>){
        mDatabase.getExecutors().diskIO().execute {
            mDatabase.dictionaryDAO().insertAll(dictionaries)
        }
    }

    fun insertDictionary(dictionaries: DictionaryEntity){
        mDatabase.getExecutors().diskIO().execute {
            mDatabase.dictionaryDAO().insert(dictionaries)
        }
    }

    fun loadDictionaryAndWords(listener: DictionaryAndWordsListener) {
        mDatabase.getExecutors().diskIO().execute {
            listener.onLoadingFinish(mDatabase.dictionaryAndWordDAO().loadDictionaryAndWord())
        }
    }

    fun loadDictionaryWithWord(listener: DictionaryWithWordsListener) {
        mDatabase.getExecutors().diskIO().execute {
            listener.onLoadingFinish(mDatabase.dictionaryAndWordDAO().loadDictionaryWithWord())
        }
    }

    interface DictionaryAndWordsListener {
        fun onLoadingFinish(list: List<IntegrationQuery.DictionaryAndWord>)
    }

    interface DictionaryWithWordsListener {
        fun onLoadingFinish(list: List<IntegrationQuery.DictionaryWithWord>)
    }
}
