package com.owen.roomapp.model

interface Word {
    fun getDictionaryUId(): String
    fun getWord(): String
    fun getAscCode(): Int
}