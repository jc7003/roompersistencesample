package com.owen.roomapp.model

interface Dictionary {
    fun getUId(): String
    fun getTitle(): String
    fun getPrice(): Int

    interface DictionaryClickCallback {
        fun onClick(dictionary: Dictionary)
    }
}