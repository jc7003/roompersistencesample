package com.owen.roomapp

import android.app.Application
import com.owen.roomapp.database.AppDatabase

class RoomApp : Application() {
    private lateinit var mAppExecutors: AppExecutors

    override fun onCreate() {
        super.onCreate()

        mAppExecutors = AppExecutors()

        getRepository()
    }

    fun getDatabase(): AppDatabase {
        return AppDatabase.getInstance(this, mAppExecutors)
    }

    fun getRepository(): DataRepository {
        return DataRepository.getInstance(getDatabase())
    }
}