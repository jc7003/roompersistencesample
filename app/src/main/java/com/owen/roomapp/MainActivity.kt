package com.owen.roomapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.owen.roomapp.database.entity.DictionaryEntity
import com.owen.roomapp.database.entity.IntegrationQuery
import com.owen.roomapp.database.entity.WordEntity
import com.owen.roomapp.ui.DictionaryListFragment
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var mRepository: DataRepository
    private val mDictionary01 = "Dictionary_01"
    private val mDictionary02 = "Dictionary_02"

    private val mTestWord = WordEntity().apply {
        this.mDictionaryUId = mDictionary02
        this.mAscCode = 100
        this.mWord = "test"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mRepository = (application as RoomApp).getRepository()
        initDictionaries()
        initWords()

        // Add product list fragment if this is first creation
        if (savedInstanceState == null) {
            val fragment = DictionaryListFragment()
            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, DictionaryListFragment.TAG).commit()
        }

//        // add word
//        findViewById<View>(R.id.button).setOnClickListener {
//            mRepository.insertWord(mTestWord)
//        }
//
//        findViewById<View>(R.id.button2).setOnClickListener {
//            mRepository.deleteWord(mTestWord)
//        }
//
//
//        logDictionaryAndWords()
//        logDictionaryWithWords()
    }

    private fun logDictionaryAndWords(){
        mRepository.loadDictionaryAndWords(object : DataRepository.DictionaryAndWordsListener {
            override fun onLoadingFinish(list: List<IntegrationQuery.DictionaryAndWord>) {
                list.map {
                    val dictionary = it.dictionary
                    Log.d("MainActivity", "1 dictionary: ${dictionary?.name}")
                    it.words?.map {
                        Log.d("MainActivity", "words: ${it.mWord}")
                    }
                }
            }
        })
    }

    private fun logDictionaryWithWords(){
        mRepository.loadDictionaryWithWord(object : DataRepository.DictionaryWithWordsListener{
            override fun onLoadingFinish(list: List<IntegrationQuery.DictionaryWithWord>) {
                list.map {
                    Log.d("MainActivity", "2 dictionary name: ${it.dictionary?.name}, words: ${it.words?.mWord}")
                }
            }
        })
    }

    private fun initDictionaries() {
        mRepository.loadDictionaries().value?.run {
            this.map {
                Log.d("MainActivity", "initDictionaries name: ${it.name}")
            }

            if (this.isNotEmpty()) {
                return
            }
        }

        val list = ArrayList<DictionaryEntity>().apply {
            add(DictionaryEntity().apply {
                name = mDictionary01
                uid = name
                mPrice = 100
            })
            add(DictionaryEntity().apply {
                name = mDictionary02
                uid = name
                mPrice = 200
            })
        }

        mRepository.insertDictionaries(list)
    }

    private fun initWords() {
        val list = ArrayList<WordEntity>().apply {
            add(WordEntity().apply {
                mAscCode = 49
                mWord = "Apple"
                mDictionaryUId = mDictionary01
            })
            add(WordEntity().apply {
                mAscCode = 50
                mWord = "Bee"
                mDictionaryUId = mDictionary01
            })
            add(WordEntity().apply {
                mAscCode = 51
                mWord = "Car"
                mDictionaryUId = mDictionary01
            })
            add(WordEntity().apply {
                mAscCode = 52
                mWord = "Desktop"
                mDictionaryUId = mDictionary01
            })

            add(WordEntity().apply {
                mAscCode = 53
                mWord = "Egg"
                mDictionaryUId = mDictionary01
            })

            add(WordEntity().apply {
                mAscCode = 54
                mWord = "Father"
                mDictionaryUId = mDictionary01
            })

            add(WordEntity().apply {
                mAscCode = 55
                mWord = "Green"
                mDictionaryUId = mDictionary01
            })

            add(WordEntity().apply {
                mAscCode = 56
                mWord = "Hello world"
                mDictionaryUId = mDictionary02
            })

            add(WordEntity().apply {
                mAscCode = 57
                mWord = "ice cream"
                mDictionaryUId = mDictionary02
            })
        }

        mRepository.insertWords(list)
    }
}
