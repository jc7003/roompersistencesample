package com.owen.roomapp.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.owen.roomapp.model.Dictionary


@Entity(tableName = "dictionary")
class DictionaryEntity : Dictionary {

    @PrimaryKey(autoGenerate = false)
    var uid: String = ""

    var name: String = ""

    @ColumnInfo(name = "price")
    var mPrice: Int = 100

    @Ignore
    override fun getUId(): String {
        return uid
    }
    @Ignore
    override fun getTitle(): String {
        return name
    }
    @Ignore
    override fun getPrice(): Int {
        return mPrice
    }
}