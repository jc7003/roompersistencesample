package com.owen.roomapp.database.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class IntegrationQuery {

    // join sample 1
    class DictionaryAndWord {
        @Embedded
        var dictionary: DictionaryEntity? = null

        @Relation(parentColumn = "uid", entityColumn = "d_uid", entity = WordEntity::class)
        var words: List<WordEntity>? = null
    }

    // join sample 2
    // use query command
    class DictionaryWithWord {
        @Embedded
        var dictionary: DictionaryEntity? = null
        @Embedded
        var words: WordEntity? = null
    }
}