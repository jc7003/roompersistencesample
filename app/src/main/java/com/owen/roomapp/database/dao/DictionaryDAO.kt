package com.owen.roomapp.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.owen.roomapp.database.entity.DictionaryEntity

@Dao
interface DictionaryDAO {

    @Query("SELECT * FROM dictionary")
    fun loadAllDictionaries(): LiveData<List<DictionaryEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(dictionary: List<DictionaryEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(dictionary: DictionaryEntity)
}