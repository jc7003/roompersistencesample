package com.owen.roomapp.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
//import com.owen.roomapp.database.entity.DictionaryAndWord
import com.owen.roomapp.database.entity.IntegrationQuery

class IntegrationDAO {

    @Dao
    interface DictionaryAndWordDAO {

        @Query("select * from dictionary")
        fun loadDictionaryAndWord(): List<IntegrationQuery.DictionaryAndWord>

        @Query("select * from dictionary")
        fun loadDictionaryAndWordWithLiveData(): LiveData<List<IntegrationQuery.DictionaryAndWord>>

        @Query("select * from dictionary inner join word_table on word_table.d_uid = dictionary.uid group by word_table.ascii  order by word_table.ascii ")
        fun loadDictionaryWithWord(): List<IntegrationQuery.DictionaryWithWord>
    }
}