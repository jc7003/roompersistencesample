package com.owen.roomapp.database

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.owen.roomapp.AppExecutors
import com.owen.roomapp.database.dao.DictionaryDAO
import com.owen.roomapp.database.dao.IntegrationDAO
import com.owen.roomapp.database.dao.WordDAO
import com.owen.roomapp.database.entity.WordEntity
import com.owen.roomapp.database.entity.DictionaryEntity

@Database(version = 3, exportSchema = false,
        entities = [WordEntity::class, DictionaryEntity::class])

public abstract class AppDatabase : RoomDatabase() {

    abstract fun wordDAO(): WordDAO
    abstract fun dictionaryDAO(): DictionaryDAO
    abstract fun dictionaryAndWordDAO(): IntegrationDAO.DictionaryAndWordDAO

    private val mIsDatabaseCreated = MutableLiveData<Boolean>()

    companion object {
        private var sInstance: AppDatabase? = null
        const val DATABASE_NAME = "RoomAppDB"
        private lateinit var sExecutors: AppExecutors

        fun getInstance(context: Context, executors: AppExecutors): AppDatabase {
            if (sInstance == null) {
                synchronized(DATABASE_NAME) {
                    if (sInstance == null) {
                        sInstance = buildDatabase(context.applicationContext, executors)
                        sInstance!!.updateDatabaseCreated(context.applicationContext)
                    }
                }
            }
            return sInstance!!
        }

        private fun buildDatabase(appContext: Context, executors: AppExecutors): AppDatabase {
            sExecutors = executors
            return Room.databaseBuilder<AppDatabase>(appContext, AppDatabase::class.java,
                    DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }

    /**
     * Check whether the database already exists and expose it via [.getDatabaseCreated]
     */
    private fun updateDatabaseCreated(context: Context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated()
        }
    }

    private fun setDatabaseCreated() {
        mIsDatabaseCreated.postValue(true)
    }

    fun getDatabaseCreated(): LiveData<Boolean> {
        return mIsDatabaseCreated
    }

    fun getExecutors(): AppExecutors {
        return sExecutors
    }
}