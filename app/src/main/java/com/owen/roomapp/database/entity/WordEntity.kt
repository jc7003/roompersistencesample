package com.owen.roomapp.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.owen.roomapp.model.Word

@Entity(tableName = "word_table")
class WordEntity {

    @PrimaryKey
    @ColumnInfo(name = "ascii")
    var mAscCode: Int = 0

    @ColumnInfo(name = "word")
    var mWord: String = ""

    @ColumnInfo(name = "d_uid")
    var mDictionaryUId : String = ""
}