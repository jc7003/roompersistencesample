package com.owen.roomapp.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.owen.roomapp.database.entity.WordEntity

@Dao
interface WordDAO {

    @Query("SELECT * FROM word_table")
    fun loadAllWords(): LiveData<List<WordEntity>>

    @Query("select * from word_table where ascii = :ascii")
    fun loadWord(ascii: Int): LiveData<WordEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(words: List<WordEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(words: WordEntity)

    @Delete
    fun delete(words: WordEntity)
}